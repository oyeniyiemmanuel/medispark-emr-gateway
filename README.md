# Setting Up Gateway
- Download and install Laragon
- Download and install composer
- Clone the repo into C:/Laragon/www folder
- cd to the newly cloned folder i.e. c:/laragon/www/gateway
- Open laragon cli and run `composer install` to install all dependencies
- rename .env-local file to .env
- run `php -S localhost:8005 -t public`

# Setting up Database
- click "database" and open a new instance
- create new database, let it match the DB_DATABASE variable in .env
- run `php artisan migrate`
- run `php artisan passport:install`
- run `php artisan db:seed`