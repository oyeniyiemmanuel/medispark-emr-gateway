<?php

return [
    'appointment_status'   =>  [
        'open'  =>  'open',
        'closed'  =>  'closed',
    ],
    'bill_type'   =>  [
        'billed'  =>  'billed',
        'not-billed'  =>  'not-billed',
    ],
    'care_type'   =>  [
        'inpatient'  =>  'inpatient',
        'outpatient'  =>  'outpatient',
        'both'  =>  'both',
    ],
    'form_fields'   =>  [
        'radio'  =>  'radio',
        'date'  =>  'date',
        'range'  =>  'range',
        'time'  =>  'time',
        'number'  =>  'number',
        'checkbox'  =>  'checkbox',
        'number'  =>  'number',
        'text'  =>  'text',
        'textarea'  =>  'textarea',
        'select'  =>  'select',
        'multi-select'  =>  'multi-select',
    ],
    'retainer_category'   =>  [
        'fee-paying'  =>  'fee-paying',
        'private-insurance'  =>  'private-insurance',
        'company-insurance'  =>  'company-insurance',
    ],
    'service_category'   =>  [
        'registration'  =>  'registration',
        'consultation'  =>  'consultation',
        'pharmacy'  =>  'pharmacy',
        'imaging'  =>  'imaging',
        'procedure'  =>  'procedure',
        'laboratory'  =>  'laboratory',
    ],
    'service_state'   =>  [
        'triggered'  =>  'triggered',
        'confirmed'  =>  'confirmed',
        'paid'  =>  'paid',
        'delivered'  =>  'delivered',
    ],
    'visit_status'   =>  [
        'new'  =>  'new',
        'old'  =>  'old',
        'none'  =>  'none',
    ],
    'visit_type'   =>  [
        'ocs'  =>  'ocs',
        'see-physician'  =>  'see-physician',
    ]
];