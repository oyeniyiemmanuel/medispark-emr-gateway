<?php

namespace App\MicroServices\Billing;

use App\Traits\ConsumeExternalMicroservice;
use Illuminate\Http\Request;

class FormService
{
    use ConsumeExternalMicroservice;

    /**
     * The base uri to consume Service service
     * @var string
     */
    public $baseUri;

    /**
     * The uri prefix to consume Service service
     * @var string
     */
    public $uriPrefix;

    /**
     * authorization secret to pass to Service api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.billing.base_uri');
        $this->uriPrefix = config('services.billing.uri_prefix');
        $this->secret = config('services.billing.secret');
    }

    /**
     * consumes billing microservice's api to add form to user
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function newForm(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/form/new', $request->all());
    }

    /**
     * consumes billing microservice's api to add fields to a form
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function addFormField(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/form/add-field', $request->all());
    }

    /**
     * consumes billing microservice's api to create new form ansers for a user
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function newUserForm(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/form/new-user-form', $request->all());
    }
}