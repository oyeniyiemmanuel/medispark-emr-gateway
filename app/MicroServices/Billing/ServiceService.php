<?php

namespace App\MicroServices\Billing;

use App\Traits\ConsumeExternalMicroservice;
use Illuminate\Http\Request;

class ServiceService
{
    use ConsumeExternalMicroservice;

    /**
     * The base uri to consume Service service
     * @var string
     */
    public $baseUri;

    /**
     * The uri prefix to consume Service service
     * @var string
     */
    public $uriPrefix;

    /**
     * authorization secret to pass to Service api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.billing.base_uri');
        $this->uriPrefix = config('services.billing.uri_prefix');
        $this->secret = config('services.billing.secret');
    }

    /**
     * consumes billing microservice's api to store new Service
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function newService(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/service/new', $request->all());
    }

    /**
     * consumes billing microservice's api to add service to user
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function addServiceToUser(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/service/add-to-user', $request->all());
    }
}