<?php

namespace App\MicroServices\Clinic;

use App\Traits\ConsumeExternalMicroservice;

class PatientService
{
    use ConsumeExternalMicroservice;

    /**
     * The base uri to consume Patients service
     * @var string
     */
    public $baseUri;

    /**
     * The uri prefix to consume Patients service
     * @var string
     */
    public $uriPrefix;

    /**
     * authorization secret to pass to Patient api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.clinic.base_uri');
        $this->uriPrefix = config('services.clinic.uri_prefix');
        $this->secret = config('services.clinic.secret');
    }


    /**
     * consumes clinic microservice's api to fetch all patients
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function obtainPatients()
    {
        return $this->performRequest('GET', $this->uriPrefix, '/patients');
    }
}