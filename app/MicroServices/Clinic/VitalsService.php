<?php

namespace App\MicroServices\Clinic;

use App\Traits\ConsumeExternalMicroservice;
use Illuminate\Http\Request;

class VitalsService
{
    use ConsumeExternalMicroservice;

    /**
     * The base uri to consume Vitals service
     * @var string
     */
    public $baseUri;

    /**
     * The uri prefix to consume Vitals service
     * @var string
     */
    public $uriPrefix;

    /**
     * authorization secret to pass to Vitals api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.clinic.base_uri');
        $this->uriPrefix = config('services.clinic.uri_prefix');
        $this->secret = config('services.clinic.secret');
    }


    /**
     * consumes clinic microservice's api to store new vitals
     * @return \App\Traits\ConsumeExternalMicroservice
     */
    public function newVitals(Request $request)
    {
        return $this->performRequest('POST', $this->uriPrefix, '/vitals/new', $request->all());
    }
}