<?php

namespace App\Http\Controllers\appointment;

use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\ApiResponse;
use App\MicroServices\Billing\AppointmentService;
use Validator;
use Auth;

class AppointmentController extends Controller
{
    use ApiResponse;
    /**
     * Service to consume appointment micro-service
     *
     * @var AppointmentService
     */
    public $appointmentService;

    public function __construct(AppointmentService $appointmentService)
    {
        $this->appointmentService = $appointmentService;
    }

    /**
     * add appointment to user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addAppointmentToUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "user_id" => "required|integer",
            "appointee_id" => "required|integer",
            "services" => "required",
            "organization_id" => "required|integer",
            "retainer_id" => "required|integer",
            "branch_id" => "required|integer",
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->appointmentService->addAppointmentToUser($request));
    }
}
