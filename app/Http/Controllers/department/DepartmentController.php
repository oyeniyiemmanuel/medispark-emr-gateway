<?php

namespace App\Http\Controllers\department;

use App\Repositories\Interfaces\DepartmentInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Validator;
use Auth;

class DepartmentController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Department repository
     *
     * @var departmentRepository
     */
    private $departmentRepository;

    /**
     * Dependency Injection of departmentRepository.
     *
     * @param  \App\Repositories\Interfaces\DepartmentInterface  $departmentRepository
     * @return void
     */
    public function __construct(DepartmentInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * create a new Department.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newDepartment(Request $request)
    {
        
    }
}