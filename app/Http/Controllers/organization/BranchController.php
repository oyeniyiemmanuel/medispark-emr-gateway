<?php

namespace App\Http\Controllers\organization;

use App\Repositories\Interfaces\BranchInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Validator;
use Auth;

class BranchController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Branch repository
     *
     * @var branchRepository
     */
    private $branchRepository;

    /**
     * Dependency Injection of branchRepository.
     *
     * @param  \App\Repositories\Interfaces\BranchInterface  $branchRepository
     * @return void
     */
    public function __construct(BranchInterface $branchRepository)
    {
        $this->branchRepository = $branchRepository;
    }

    /**
     * create a new Branch.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newBranch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForGateway($this->branchRepository->newBranch($request));
    }
}