<?php

namespace App\Http\Controllers\patient;

use App\Repositories\Interfaces\PatientInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\ApiResponse;
use Validator;
use Auth;

class PatientController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Patient repository
     *
     * @var patientRepository
     */
    private $patientRepository;

    /**
     * Dependency Injection of patientRepository.
     *
     * @param  \App\Repositories\Interfaces\PatientInterface  $patientRepository
     * @return void
     */
    public function __construct(PatientInterface $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }

    /**
     * create a new Patient.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newPatient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required',
            'retainer_id' => 'required',
            'last_name' => 'required',
            'first_name' => 'required',
            'dob' => 'required|date',
            'phone' => 'required|unique:users,phone',
            'email' => 'email|unique:users,email',
            'services' => 'sometimes|array',
            'visit_status' => ['sometimes', 'required', Rule::in(config('enums.visit_status'))],
        ]);

        // validate that visit_id is required if the service is to be connected to an old visit
        $validator->sometimes('visit_id', 'required|integer', function ($input) {
            return $input->visit_status == 'old';
        });

        // validate that visit_type is required if the service is to be connected to a new visit
        $validator->sometimes('visit_type', ['required', Rule::in(config('enums.visit_type'))], function ($input) {
            return $input->visit_status == 'new';
        });

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForGateway($this->patientRepository->newPatient($request));
    }
}