<?php

namespace App\Http\Controllers\staff;

use App\Repositories\Interfaces\StaffInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Validator;
use Auth;

class StaffController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Staff repository
     *
     * @var staffRepository
     */
    private $staffRepository;

    /**
     * Dependency Injection of staffRepository.
     *
     * @param  \App\Repositories\Interfaces\StaffInterface  $staffRepository
     * @return void
     */
    public function __construct(StaffInterface $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    /**
     * create a new Staff.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newStaff(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'roles' => 'required',
            'branch_id' => 'required',
            'department_id' => 'required',
            'last_name' => 'required',
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|min:3|unique:users,username'
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForGateway($this->staffRepository->newStaff($request));
    }
}