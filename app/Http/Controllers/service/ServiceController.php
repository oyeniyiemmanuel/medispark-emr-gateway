<?php

namespace App\Http\Controllers\service;

use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\ApiResponse;
use App\MicroServices\Billing\ServiceService;
use Validator;
use Auth;

class ServiceController extends Controller
{
    use ApiResponse;
    /**
     * Service to consume Service micro-service
     *
     * @var serviceService
     */
    public $serviceService;

    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    /**
     * store new Service
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newService(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "amount" => "required|integer",
            "fee_paying_retainers" => "required",
            "category" => ["required", Rule::in(config('enums.service_category'))],
            "care_type" => ["required", Rule::in(config('enums.care_type'))],
            "branch_id" => "required"
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }
        
        // add organization_id attribute to the request array
        $request->request->add(["organization_id" => Auth::user()->organization->id]);

        return $this->successResponseForMicroservice($this->serviceService->newService($request));
    }

    /**
     * add service to user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addServiceToUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "user_id" => "required|integer",
            "services" => "required",
            "organization_id" => "required|integer",
            "retainer_id" => "required|integer",
            "branch_id" => "required|integer",
            "visit_status" => ["sometimes", "required", Rule::in(config('enums.visit_status'))],
            "retainer_category" => ["required", Rule::in(config('enums.retainer_category'))],
        ]);

        // validate that visit_id is required if the service is to be connected to an old visit
        $validator->sometimes('visit_id', 'required|integer', function ($input) {
            return $input->visit_status == 'old';
        });

        // validate that visit_type is required if the service is to be connected to a new visit
        $validator->sometimes('visit_type', ['required', Rule::in(config('enums.visit_type'))], function ($input) {
            return $input->visit_status == 'new';
        });

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->serviceService->addServiceToUser($request));
    }
}
