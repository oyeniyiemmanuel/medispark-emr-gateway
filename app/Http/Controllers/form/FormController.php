<?php

namespace App\Http\Controllers\form;

use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\ApiResponse;
use App\MicroServices\Billing\FormService;
use Validator;
use Auth;

class FormController extends Controller
{
    use ApiResponse;
    /**
     * Service to consume Form micro-service
     *
     * @var formService
     */
    public $formService;

    public function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    /**
     * create new form
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "organization_id" => "required|integer",
            "branch_id" => "required|integer",
            "service_category" => ["sometimes", "required", "required_with:services", Rule::in(config('enums.service_category'))],
            "services" => "required_with:service_category|array",
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->formService->newForm($request));
    }

    /**
     * create new form answers for a user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newUserForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "form_id" => "required|integer",
            "organization_id" => "required|integer",
            "branch_id" => "required|integer",
            "form_id" => "required|integer",
            "form_fields" => ["required", "array", Rule::in(config('enums.form_fields'))],
        ]);

        // validate that form_number_question_id and value are required if the request contains form_field of type number
        $validator->sometimes(['number_answers', 'number_answers.*.form_number_question_id', 'number_answers.*.value'], 'required', function ($input) {
            return in_array('number', $input->form_fields);
        });

        // validate that form_text_question_id and value are required if the request contains form_field of type text
        $validator->sometimes(['text_answers', 'text_answers.*.form_text_question_id', 'text_answers.*.value'], 'required', function ($input) {
            return in_array('text', $input->form_fields);
        });

        // validate that form_radio_question_id and value are required if the request contains form_field of type radio
        $validator->sometimes(['radio_answers', 'radio_answers.*.form_radio_question_id', 'radio_answers.*.value'], 'required', function ($input) {
            return in_array('radio', $input->form_fields);
        });

        // validate that form_select_question_id and value are required if the request contains form_field of type select
        $validator->sometimes(['select_answers', 'select_answers.*.form_select_question_id', 'select_answers.*.value'], 'required', function ($input) {
            return in_array('select', $input->form_fields);
        });

        // validate that form_multi_select_question_id and value are required if the request contains form_field of type multi_select
        $validator->sometimes(['multi_select_answers', 'multi_select_answers.*.form_multi_select_question_id', 'multi_select_answers.*.value'], 'required', function ($input) {
            return in_array('multi-select', $input->form_fields);
        });

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->formService->newUserForm($request));
    }

    /**
     * add Form fields
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addFormField(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "form_id" => "required|integer",
            "form_fields" => ["required", "array", Rule::in(config('enums.form_fields'))],
        ]);

        // validate that number name is required if the request contains form_field of type number
        $validator->sometimes(['number_details', 'number_details.*.name'], 'required', function ($input) {
            return in_array('number', $input->form_fields);
        });

        // validate that text name is required if the request contains form_field of type text
        $validator->sometimes(['text_details', 'text_details.*.name'], 'required', function ($input) {
            return in_array('text', $input->form_fields);
        });

        // validate that radio name and options are required if the request contains form_field of type radio
        $validator->sometimes(['radio_details', 'radio_details.*.name', 'radio_details.*.options'], 'required', function ($input) {
            return in_array('radio', $input->form_fields);
        });

        // validate that select name and options are required if the request contains form_field of type select
        $validator->sometimes(['select_details', 'select_details.*.name', 'select_details.*.options'], 'required', function ($input) {
            return in_array('select', $input->form_fields);
        });

        // validate that multi_select name and options are required if the request contains form_field of type multi_select
        $validator->sometimes(['multi_select_details', 'multi_select_details.*.name', 'multi_select_details.*.options'], 'required', function ($input) {
            return in_array('multi-select', $input->form_fields);
        });

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->formService->addFormField($request));
    }
}
