<?php

namespace App\Http\Controllers\retainer;

use App\Repositories\Interfaces\RetainerInterface;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Validator;
use Auth;

class RetainerController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of retainer repository
     *
     * @var retainerRepository
     */
    private $retainerRepository;

    /**
     * Dependency Injection of retainerRepository.
     *
     * @param  \App\Repositories\Interfaces\RetainerInterface  $retainerRepository
     * @return void
     */
    public function __construct(RetainerInterface $retainerRepository)
    {
        $this->retainerRepository = $retainerRepository;
    }

    /**
     * get details of the retainer.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function getDetails(Request $request)
    {
        return $this->successResponseForGateway($this->retainerRepository->getDetails($request));
    }

    /**
     * create a new retainer.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newRetainer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'branch_id' => 'required|integer',
            "category" => ["required", Rule::in(config('enums.retainer_category'))],
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForGateway($this->retainerRepository->newRetainer($request));
    }

    /**
     * get all the retainers of a branch.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function getAllBranchRetainers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required|integer',
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForGateway($this->retainerRepository->getAllBranchRetainers($request->branch_id));
    }
}