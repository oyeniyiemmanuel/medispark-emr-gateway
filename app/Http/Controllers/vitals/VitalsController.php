<?php

namespace App\Http\Controllers\Vitals;

use App\Http\Controllers\Controller;
use App\Model\Vitals;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use App\MicroServices\Clinic\VitalsService;
use Validator;

class VitalsController extends Controller
{
    use ApiResponse;
    /**
     * Service to consume Vitals micro-service
     *
     * @var VitalsService
     */
    public $vitalsService;

    public function __construct(VitalsService $vitalsService)
    {
        $this->vitalsService = $vitalsService;
    }

    /**
     * store new vitals
     *
     * @return \App\Traits\ApiResponse
     */
    public function newVitals(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "patient" => "required|integer",
            "saved_by" => "required|integer",
            "temperature" => "integer",
            "pulse" => "integer",
            "respiration" => "integer",
            "oxygen_saturation" => "integer",
            "weight" => "integer",
            "height" => "integer",
            "systolic_bp" => "integer",
            "diastolic_bp" => "integer",
            "body_mass_index" => "integer",
            "pain_score" => "integer",
        ]);

        if($validator->fails()){
            return $this->errorResponseWithDetails('validation failed', $validator->errors(), 400);
        }

        return $this->successResponseForMicroservice($this->vitalsService->newVitals($request));
    }
}
