<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;

class InstallationLocationRoutesMiddleware
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * filters requests to allow routes only when app is installed on the cloud.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (env('APP_INSTALLATION_LOCATION') !== 'cloud') {
            return $this->errorResponseWithoutDetails('route only allowed for cloud installation', 403);
        }

        return $next($request);
    }
}
