<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Auth;

class UserHasBranchMiddleware
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * filters requests to allow routes only when app is installed on the cloud.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->branch == null) {
            return $this->errorResponseWithoutDetails('User has no branch', 403);
        }

        return $next($request);
    }
}
