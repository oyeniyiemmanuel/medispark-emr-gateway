<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\StaffInterface;
use App\Repositories\Interfaces\OrganizationInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Organization;
use App\Models\Branch;
use App\Models\Staff;
use App\Models\User;
use Auth;

class OrganizationRepository implements OrganizationInterface
{
    /**
     * private declaration of repositories
     *
     * @var userRepository
     * @var staffRepository
     */
    private $userRepository;
    private $staffRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $userRepository
     * @param  \App\Repositories\Interfaces\StaffInterface  $staffRepository
     * @return void
     */
    public function __construct(UserInterface $userRepository, StaffInterface $staffRepository)
    {
        $this->userRepository = $userRepository;
        $this->staffRepository = $staffRepository;
    }

	/**
     * get organization's details
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getDetails(Request $request)
    {
        $data['message'] = "got to clinic/organization/details page";
        return $data;   
    }

    // /**
    //  * login user to a branch of an organization
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return App\Models\Organization
    //  */
    // public function login(Request $request)
    // {
        
    //     // find user using the username, organization_id, and branch_id
    //     $user = User::where([
    //             ['organization_id', '=', $request->organization_id],
    //             ['branch_id', '=', $request->branch_id],
    //             ['username', '=', $request->username]
    //         ])->first();

    //     $result = [];

    //     // return error if the above search does not produce a corresponding result
    //     if ($user == null) {
    //        $result['status'] = 'error';
    //        $result['message'] = 'user not found';
    //        return $result;
    //     }

    //     // verify the password against a hash
    //     if (!Hash::check($request->password, $user->password)) {
    //         $result['status'] = 'error';
    //         $result['message'] = 'password incorrect';
    //         return $result;
    //     }

    //     // create login data to be used for generating a token
    //     $loginData = [];
    //     $loginData['grant_type'] = 'password';
    //     $loginData['client_id'] = 2;
    //     $loginData['client_secret'] = env('PASSPORT_CLIENT_2');
    //     $loginData['username'] = $user->id;
    //     $loginData['password'] = $request->password;

    //     // redirect to oauth url that will generate a token, 
    //     // NOTE THAT REDIRECT DOES NOT WORK WITH OTHER DATA TYPES THAN INTEGER
    //     return redirect('oauth/token', json_encode($loginData));
    // }

    /**
     * create new organization
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\Organization
     */
    public function newOrganization(Request $request)
    {
        // create new organization
        $new_organization = Organization::firstOrCreate([
                        "name" => $request->name,
                        "street" => $request->street,
                        "city" => $request->city,
                        "state" => $request->state,
                        "email" => $request->email,
                        "website" => $request->website,
                        "phone" => $request->phone,
                    ]);

        // create new staff
        $staff = new Staff;
        $staff->save();

        // create a new branch
        $branch = new Branch;
        $branch->street = $new_organization->street;
        $branch->city = $new_organization->city;
        $branch->state = $new_organization->state;
        $branch->save();

        // make a slug and update new organization details
        $new_organization->slug = $this->makeSlug($new_organization->name);
        $new_organization->save();

        // attach organization to user
        $this->addUserToOrganization(Auth::id(), $new_organization->id);

        // attach as one of user's branches
        $branch->users()->syncWithoutDetaching(Auth::user());  

        // attach as default branch to user 
        $branch->user()->save(Auth::user());   

        // set the first created branch as headquarter
        $this->setBranchAsOrganizationHQ($branch->id, $new_organization->id);

        // attach branch to organization
        $this->addBranchToOrganization($branch->id, $new_organization->id);

        // attach user to staff
        $this->staffRepository->addUserToStaff(Auth::id(), $staff->id);

        return $new_organization;   
    }

    /**
   
     * Get User's organization details
     * @param  int $user_id
     * @return App\Models\Organization
     */
    public function getOrganizationByUserId($user_id)
    {
        return User::findOrFail($user_id)->organization;
    }

    /**
     * attach user to a organization
     * @param  int  $user_id
     * @param  int  $organization_id
     * @return void
     */
    public function addUserToOrganization($user_id, $organization_id)
    {
        // get organization
        $organization = $this->getOrganizationById($organization_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // sync user to the organization without removing previous users, also don't sync if its done before
        $organization->users()->save($user);   
    }

    /**
     * attach branch to an organization
     * @param  int  $branch_id
     * @param  int  $organization_id
     * @return void
     */
    public function addBranchToOrganization($branch_id, $organization_id)
    {
        // get organization
        $organization = $this->getOrganizationById($organization_id);

        // get branch
        $branch = Branch::findOrFail($branch_id);

        // attach branch to organization
        $organization->branches()->save($branch);   
    }

    /**
     * set branch as an organization's Headquarter
     * @param  int  $branch_id
     * @param  int  $organization_id
     * @return void
     */
    public function setBranchAsOrganizationHQ($branch_id, $organization_id)
    {
        // get organization
        $organization = $this->getOrganizationById($organization_id);

        // update hq branch id of the organization
        $organization->hq_branch_id = $branch_id;
        $organization->save();   
    }

    /**
     * Get organization using ID
     * @param  int $organization_id
     * @return App\Models\Organization
     */
    public function getOrganizationById($organization_id)
    {
        return Organization::find($organization_id)->with("branches")->first();
    }

    /**
     * Get organization using slug
     * @param  int $organization_slug
     * @return App\Models\Organization
     */
    public function getOrganizationBySlug($organization_slug)
    {
        return Organization::where("slug", $organization_slug)->with("branches")->first();
    }

    /**
     * create slug for organization
     * @param  int $text
     * @return string
     */
    public function makeSlug($text)
    {
        $slug = Str::slug( substr($text, 0, 150) );

        $latestSlug = 
            Organization::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $slug.='-'.($number + 1);
        }  
        
        return $slug;
    }
}