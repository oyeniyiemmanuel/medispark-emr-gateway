<?php

namespace App\Repositories;

use App\Repositories\Interfaces\OrganizationInterface;
use App\Repositories\Interfaces\BranchInterface;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\User;
use Auth;

class BranchRepository implements BranchInterface
{
    /**
     * private declaration of repositories
     *
     * @var userRepository
     * @var organizationRepository
     */
    private $userRepository;
    private $organizationRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $userRepository
     * @param  \App\Repositories\Interfaces\OrganizationInterface  $organizationRepository
     * @return void
     */
    public function __construct(
                            OrganizationInterface $organizationRepository, 
                            UserInterface $userRepository
                            )
    {
        $this->organizationRepository = $organizationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * create new branch
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newBranch(Request $request)
    {
        // get organization details
        $organization = $this->organizationRepository->getOrganizationByUserId(Auth::id());

        // create new branch if it doesn't exist
        $branch = Branch::firstOrCreate($request->all());

        // attach branch to user
        $this->addUserToBranch(Auth::id(), $branch->id);

        // set user's default branch
        $this->setUserDefaultBranch(Auth::id(), $branch->id);

        // attach branch to organization
        $this->organizationRepository->addBranchToOrganization($branch->id, $organization->id);

        // return newly created branch
        return $branch;   
    }

    /**
     * Get branch using ID
     * @param  int $branch_id
     * @return array
     */
    public function getBranchById($branch_id)
    {
        return Branch::findOrFail($branch_id);
    }

    /**
     * attach user to a branch
     * @param  int  $user_id
     * @param  int  $branch_id
     * @return void
     */
    public function addUserToBranch($user_id, $branch_id)
    {
        // get branch
        $branch = $this->getBranchById($branch_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // attach as one of user's branches: sync user to the branch without removing previous users, also don't sync if its done before
        $branch->users()->syncWithoutDetaching($user);   
    }

    /**
     * set user default branch
     * @param  int  $user_id
     * @param  int  $branch_id
     * @return void
     */
    public function setUserDefaultBranch($user_id, $branch_id)
    {
        // get branch
        $branch = $this->getBranchById($branch_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // attach as default branch to user 
        $branch->user()->save($user);   
    }

    /**
   
     * Get User's branch details
     * @param  int $user_id
     * @return array
     */
    public function getBranchByUserId($user_id)
    {
        return User::findOrFail($user_id)->branch;
    }
}