<?php

namespace App\Repositories;

use App\Repositories\Interfaces\DepartmentInterface;
use App\Repositories\Interfaces\StaffInterface;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Department;
use App\Models\Retainer;
use App\Models\Branch;
use App\Models\Staff;
use App\Models\User;
use Auth;

class StaffRepository implements StaffInterface
{
    /**
     * private declaration of repositories
     *
     * @var organizationRepository
     */
    private $userRepository;
    private $departmentRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $userRepository
     * @param  \App\Repositories\Interfaces\DepartmentInterface  $departmentRepository
     * @return void
     */
    public function __construct(UserInterface $userRepository, DepartmentInterface $departmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
    }

	/**
     * get Retainer's details
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getDetails(Request $request)
    {
        $data['message'] = "got to clinic/patient/details page";
        return $data;   
    }

    /**
     * create new staff
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newStaff(Request $request)
    {
        // add password attribute to the request array and make it user's lastname by default
        $request->request->add(["password" => $request->last_name]);

        // create new user
        $user_id = $this->userRepository->register($request)['id'];
        $user = $this->userRepository->getUserById($user_id);

        // create new staff
        $staff = new Staff;
        $staff->save();

        // attach user to staff
        $this->addUserToStaff($user->id, $staff->id);

        // attach roles if any
        $request->filled('roles')? $user->assignRole($request->roles): null;

        // get branch
        $branch = Branch::findOrFail($request->branch_id);

        // get admin's organization
        $organization = Organization::findOrFail(Auth::user()->organization_id);

        // get department
        $department = Department::findOrFail($request->department_id);

        // attach user to department
        $this->departmentRepository->addUserToDepartment($user->id, $department->id);

        // attach as one of user's branches
        $branch->users()->syncWithoutDetaching($user->id);  

        // attach as default branch to user 
        $branch->user()->save($user);

        // sync user to the organization without removing previous users, also don't sync if its done before
        $organization->users()->save($user);

        $user->refresh();

        return $user;
    }

    /**
     * Get retainer using ID
     * @param  int $retainer_id
     * @return array
     */
    public function getStaffById($staff_id)
    {
        return Staff::findOrFail($staff_id);
    }

    /**
     * attach user to a staff
     * @param  int  $user_id
     * @param  int  $staff_id
     * @return void
     */
    public function addUserToStaff($user_id, $staff_id)
    {
        // get staff
        $staff = $this->getStaffById($staff_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // sync user to the staff without removing previous users, also don't sync if its done before
        $staff->user()->save($user);   
    }

    /**
   
     * Get User's staff details
     * @param  int $user_id
     * @return array
     */
    public function getStaffByUserId($user_id)
    {
        return User::findOrFail($user_id)->staff;
    }
}