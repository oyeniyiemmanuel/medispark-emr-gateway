<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\BranchInterface;
use App\Repositories\Interfaces\PatientInterface;
use App\MicroServices\Billing\ServiceService;
use Illuminate\Http\Request;
use App\Models\Retainer;
use App\Models\Patient;
use App\Models\Branch;
use App\Models\Organization;
use App\Models\User;
use Auth;

class PatientRepository implements PatientInterface
{
    /**
     * private declaration of repositories
     *
     * @var organizationRepository
     * @var serviceService
     */
    private $userRepository;
    private $serviceService;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $userRepository
     * @return void
     */
    public function __construct(UserInterface $userRepository, ServiceService $serviceService)
    {
        $this->userRepository = $userRepository;
        $this->serviceService = $serviceService;
    }

	/**
     * get patient's details
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getDetails(Request $request)
    {
        $data['message'] = "got to clinic/patient/details page";
        return $data;   
    }

    /**
     * create new patient
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newPatient(Request $request)
    {
        // add password attribute to the request array and make it user's lastname by default
        $request->request->add(["password" => $request->last_name]);

        // create new user
        $user_id = $this->userRepository->register($request)['id'];
        $user = $this->userRepository->getUserById($user_id);

        // create new patient
        $patient = new Patient;
        $patient->save();

        // attach user to patient
        $this->addUserToPatient($user->id, $patient->id);

        // get branch
        $branch = Branch::findOrFail($request->branch_id);

        // get logged in user's organization
        $organization = Organization::findOrFail(Auth::user()->organization_id);

        // get retainer
        $retainer = Retainer::findOrFail($request->retainer_id);

        // attach as one of user's retainers
        $retainer->users()->syncWithoutDetaching($user);  

        // attach as one of user's branches
        $branch->users()->syncWithoutDetaching($user->id);  

        // attach as default branch to user 
        $branch->user()->save($user);

        // sync user to the organization without removing previous users, also don't sync if its done before
        $organization->users()->save($user);

        // add services to user if request has any
        if ($request->filled('services')) {
            //extra attributes added to the request array
            $request->request->add(["user_id" => $user->id]);
            $request->request->add(["organization_id" => $organization->id]);
            $request->request->add(["retainer_category" => $retainer->category]);

            $this->serviceService->addServiceToUser($request);
        }

        $user->refresh();

        return $user;
    }

    /**
     * Get service using ID
     * @param  int $service_id
     * @return array
     */
    public function getPatientById($patient_id)
    {
        return Patient::findOrFail($patient_id);
    }

    /**
     * attach user to a Patient
     * @param  int  $user_id
     * @param  int  $patient_id
     * @return void
     */
    public function addUserToPatient($user_id, $patient_id)
    {
        // get Patient
        $Patient = $this->getPatientById($patient_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // sync user to the Patient without removing previous users, also don't sync if its done before
        $Patient->user()->save($user);   
    }

    /**
   
     * Get User's Patient details
     * @param  int $user_id
     * @return array
     */
    public function getPatientByUserId($user_id)
    {
        return User::findOrFail($user_id)->patient;
    }
}