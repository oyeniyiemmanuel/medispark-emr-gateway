<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Organization;

class UserRepository implements UserInterface
{
	/**
     * Register a new user.
     * - Strip names to lowercase
     * - Persist incoming request to db
     * - generate access token
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function register(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make(strtolower($input['password']));

        // strip some variables to lowercase
        $request->first_name? $input['first_name'] = strtolower($input['first_name']): null ;
        $request->last_name? $input['last_name'] = strtolower($input['last_name']): null ;
        $request->middle_name? $input['middle_name'] = strtolower($input['middle_name']): null ;
        $request->email? $input['email'] = strtolower($input['email']): null ;
        $request->username? $input['username'] = strtolower($input['username']): null ;

        // persist incoming request to db
		$user = User::create($input);
      
        // access token is generated here
        $data['id'] =  $user->id;
        $data['token'] =  $user->createToken('Medispark EMR')->accessToken;
        $data['last_name'] =  $user->last_name;
        $data['email'] =  $user->email;
        $data['username'] =  $user->username;

        // Return response
        return $data;
    }
    
    /**
     * Login user.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function login(Request $request)
    {
        // find user via identifier
        $user = User::where([
                ['organization_id', '=', $request->organization_id],
                ['branch_id', '=', $request->branch_id],
                ['email', '=', $request->identifier]
            ])->orWhere([
                ['organization_id', '=', $request->organization_id],
                ['branch_id', '=', $request->branch_id],
                ['username', '=', $request->identifier]
            ])->first();

        if ($user == null) {
            $data["status"] = "error";
            $data["message"] = "can not find user, check your email or username again";
            return $data;
        }

        if (!Hash::check($request->password, $user->password)) {
            $data["status"] = "error";
            $data["message"] = "incorrect password";
            return $data;
        }

        $roles = $user->getRoleNames();
        $access_token = $user->createToken('authToken')->accessToken;
        $data['status'] = "success";
        // remove roles collection from user model
        unset($user['roles']);
        $data['user'] = $user;
        $data['user']['roles'] = $roles;
        $data['access_token'] = $access_token;

        // Return response
        return $data;
    }

    /**
   
     * Get basic details of the user that are needed by other microservices
     * @param  int $user_id
     * @return array
     */
    public function getApiDetails($user_id)
    {
        return User::findOrFail($user_id);
    }

    /**
   
     * Get User using ID
     * @param  int $user_id
     * @return array
     */
    public function getUserById($user_id)
    {
        return User::findOrFail($user_id);
    }
}