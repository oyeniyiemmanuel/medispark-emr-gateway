<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\BranchInterface;
use App\Repositories\Interfaces\RetainerInterface;
use App\Repositories\Interfaces\OrganizationInterface;
use Illuminate\Http\Request;
use App\Models\Retainer;
use App\Models\User;
use Auth;

class RetainerRepository implements RetainerInterface
{
    /**
     * private declaration of repositories
     *
     * @var organizationRepository
     */
    private $userRepository;
    private $branchRepository;
    private $organizationRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\OrganizationInterface  $organizationRepository
     * @param  \App\Repositories\Interfaces\BranchInterface  $branchRepository
     * @param  \App\Repositories\Interfaces\UserInterface  $userRepository
     * @return void
     */
    public function __construct(
                            OrganizationInterface $organizationRepository, 
                            BranchInterface $branchRepository,
                            UserInterface $userRepository
                        )
    {
        $this->userRepository = $userRepository;
        $this->branchRepository = $branchRepository;
        $this->organizationRepository = $organizationRepository;
    }

	/**
     * get Retainer's details
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getDetails(Request $request)
    {
        $data['message'] = "got to clinic/organization/retainer/details page";
        return $data;   
    }

    /**
     * create new Retainer
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newRetainer(Request $request)
    {
        // persist new Retainer details
        $new_retainer = Retainer::firstOrCreate([
                        "name" => $request->name,
                        "street" => $request->street,
                        "city" => $request->city,
                        "state" => $request->state,
                        "country" => $request->country,
                        "category" => $request->category,
                    ]);
        // get user's organization
        $organization = $this->organizationRepository->getOrganizationByUserId(Auth::id());

        // get designated branch
        $branch = $this->branchRepository->getBranchById($request->branch_id);

        // set user's default retainer
        $this->setUserDefaultRetainer(Auth::id(), $new_retainer->id);

        // attach retainer to organization
        $organization->retainers()->save($new_retainer);

        // attach retainer to branch
        $branch->retainers()->save($new_retainer);

        // attach retainer to user
        $this->addUserToRetainer(Auth::id(), $new_retainer->id);

        return $new_retainer;   
    }

    /**
     * Get retainer using ID
     * @param  int $retainer_id
     * @return array
     */
    public function getRetainerById($retainer_id)
    {
        return Retainer::findOrFail($retainer_id);
    }

    /**
     * get all the retainers of a branch.
     * @param  int $branch_id
     * @return array
     */
    public function getAllBranchRetainers($branch_id)
    {
        $branch = $this->branchRepository->getBranchById($branch_id);

        return $branch->retainers;
    }

    /**
     * attach user to a retainer
     * @param  int  $user_id
     * @param  int  $retainer_id
     * @return void
     */
    public function addUserToRetainer($user_id, $retainer_id)
    {
        // get retainer
        $retainer = $this->getRetainerById($retainer_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // sync user to the retainer without removing previous users, also don't sync if its done before
        $retainer->users()->syncWithoutDetaching($user);   
    }

    /**
   
     * Get User's Retainer details
     * @param  int $user_id
     * @return array
     */
    public function getRetainerByUserId($user_id)
    {
        return User::findOrFail($user_id)->retainer;
    }

    /**
     * set user default retainer
     * @param  int  $user_id
     * @param  int  $retainer_id
     * @return void
     */
    public function setUserDefaultRetainer($user_id, $retainer_id)
    {
        // get retainer
        $retainer = $this->getRetainerById($retainer_id);

        // get user
        $user = $this->userRepository->getUserById($user_id);

        // attach as default retainer to user 
        $retainer->user()->save($user);   
    }
}