<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Retainer;

interface RetainerInterface
{
    public function getDetails(Request $request);

    public function newRetainer(Request $request);

    public function addUserToRetainer($user_id, $retainer_id);

    public function getRetainerById($retainer_id);

    public function getRetainerByUserId($user_id);

    public function setUserDefaultRetainer($user_id, $retainer_id);

}