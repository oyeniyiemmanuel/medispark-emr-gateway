<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Branch;

interface BranchInterface
{
    public function newBranch(Request $request);

    public function getBranchById($branch_id);

    public function addUserToBranch($user_id, $branch_id);

    public function getBranchByUserId($user_id);
    
    public function setUserDefaultBranch($user_id, $branch_id);
}