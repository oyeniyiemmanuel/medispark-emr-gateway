<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Staff;

interface StaffInterface
{
    public function getDetails(Request $request);

    public function newStaff(Request $request);

    public function addUserToStaff($user_id, $staff_id);

    public function getStaffById($staff_id);

    public function getStaffByUserId($user_id);
}