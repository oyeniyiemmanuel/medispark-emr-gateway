<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Department;

interface DepartmentInterface
{
    public function newDepartment(Request $request);

    public function getDepartmentById($department_id);

    public function addUserToDepartment($user_id, $department_id);
}