<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Organization;

interface OrganizationInterface
{
    public function getDetails(Request $request);

    public function newOrganization(Request $request);

    // public function login(Request $request);

    public function getOrganizationByUserId($user_id);

    public function getOrganizationById($organization_id);

    public function getOrganizationBySlug($organization_slug);

    public function addBranchToOrganization($branch_id, $organization_id);

    public function addUserToOrganization($user_id, $organization_id);

    public function setBranchAsOrganizationHQ($branch_id, $organization_id);

}