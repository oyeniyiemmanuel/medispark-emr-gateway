<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Patient;

interface PatientInterface
{
    public function getDetails(Request $request);

    public function newPatient(Request $request);

    public function addUserToPatient($user_id, $patient_id);

    public function getPatientById($patient_id);

    public function getPatientByUserId($user_id);
}