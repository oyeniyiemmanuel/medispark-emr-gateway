<?php

namespace App\Repositories;

use App\Repositories\Interfaces\DepartmentInterface;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\User;
use Auth;

class DepartmentRepository implements DepartmentInterface
{

    /**
     * create new Department
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newDepartment(Request $request)
    {
        return 'got to new department page';  
    }

    /**
     * Get department using id
     * @param  int $department_id
     * @return App\Model\Department
     */
    public function getDepartmentById($department_id)
    {
        return Department::findOrFail($department_id);
    }

    /**
     * attach user to a department
     * @param  int  $user_id
     * @param  int  $department_id
     * @return void
     */
    public function addUserToDepartment($user_id, $department_id)
    {
        // get department
        $department = $this->getDepartmentById($department_id);

        // get user
        $user = User::findOrFail($user_id);

        // attach user to department
        $department->users()->save($user);  
    }
}