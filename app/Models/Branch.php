<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the user's default branch.
     */
    public function user()
    {
        return $this->hasOne('App\Models\Branch');
    }

    /**
     * Get all the users of a branch.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * Get the organization a branch belongs to.
     */
    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');
    }

    /**
     * Get the retainers of a branch.
     */
    public function retainers()
    {
        return $this->hasMany('App\Models\Retainer');
    }
}
