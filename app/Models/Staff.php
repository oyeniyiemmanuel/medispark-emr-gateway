<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'staffs';

    /**
     * Get user associated with the staff.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
