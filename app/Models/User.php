<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Spatie\Permission\Traits\HasRoles;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasRoles, HasApiTokens, Authenticatable, Authorizable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ["password_confirmation"];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Find the user instance for the given identifier using either username or email.
     * @param  string  $identifier
     * @return \App\Models\User
     */
    public function findForPassport($loginData) {
        // return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
        // return $this->where('id', $identifier)->first();

        return $this->where([
                ['organization_id', '=', $loginData["organization_id"]],
                ['branch_id', '=', $loginData["branch_id"]],
                ['email', '=', $loginData["identifier"]]
            ])->orWhere([
                ['organization_id', '=', $loginData["organization_id"]],
                ['branch_id', '=', $loginData["branch_id"]],
                ['username', '=', $loginData["identifier"]]
            ])->first();
    }

    /**
     * Get the organization a user belongs to.
     */
    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');
    }

    /**
     * Get the user's default branch.
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /**
     * Get the user's default retainer.
     */
    public function retainer()
    {
        return $this->belongsTo('App\Models\Retainer');
    }

    /**
     * Get the user's department.
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    /**
     * Get the user associated with the patient.
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * Get the user associated with the staff.
     */
    public function staff()
    {
        return $this->belongsTo('App\Models\Staff');
    }

    /**
     * Get all the branches of a user.
     */
    public function branches()
    {
        return $this->belongsToMany('App\Models\Branch');
    }

    /**
     * Get all the retainers of a user.
     */
    public function retainers()
    {
        return $this->belongsToMany('App\Models\Retainer');
    }
}
