<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Branch;

class Organization extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the users of an organization.
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    /**
     * Get the retainers of an organization.
     */
    public function retainers()
    {
        return $this->hasMany('App\Models\Retainer');
    }

    /**
     * Accessor to get the headquarter of an organization.
     */
    public function getHeadquarterAttribute()
    {
        return Branch::find($this->hq_branch_id);
    }

    /**
     * Get the branches of an organization.
     */
    public function branches()
    {
        return $this->hasMany('App\Models\Branch');
    }
}
