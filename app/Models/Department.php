<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the users of a Department.
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
