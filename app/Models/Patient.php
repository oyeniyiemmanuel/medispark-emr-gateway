<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get user associated with the patient.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
