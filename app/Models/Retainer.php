<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Retainer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the organization a retainer belongs to.
     */
    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');
    }

    /**
     * Get the branch a retainer belongs to.
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /**
     * Get the user's default retainer.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    /**
     * Get all the users of a Retainer.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}