<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumeExternalMicroservice
{
    /**
     * Send request to any service
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return string
     */
    public function performRequest($method, $uriPrefix, $requestUrl, $formParams = [], $headers = [])
    {
        $client = new Client([
            'base_uri'  =>  $this->baseUri,
        ]);

        if(isset($this->secret))
        {
            $headers['Authorization'] = $this->secret;
        }

        // concatenate microservice prefix with request url e.g. /clinic/patient/all where /clinic is the microservice and /patient/all is the request url
        $requestUrl = "/".$uriPrefix.$requestUrl;

        $response = $client->request($method, $requestUrl, [
            'json' => $formParams,
            'headers'     => $headers,
        ]);

        return $response->getBody()->getContents();
    }
}