<?php

namespace App\Traits;

trait ApiResponse
{
    /**
     * Success Response
     * @param array $data
     * @param boolean $status
     * @param int $code
     * @return json
     */
    public function successResponseForMicroservice($data, $status = true, $code = \Illuminate\Http\Response::HTTP_OK)
    {
        return response($data)->header('Content-Type', 'application/json');
    }

    /**
     * Success Response
     * @param array $data
     * @param boolean $status
     * @param int $code
     * @return json
     */
    public function successResponseForGateway($data, $status = true, $service = "", $code = \Illuminate\Http\Response::HTTP_OK)
    {
        $service = env('MICROSERVICE_NAME');
        
        return response()->json(
                        [
                            'code' => $code,
                            'status' => $status,
                            'service' => $service,
                            'data' => $data
                        ], 
                        $code
                    )->header('Content-Type', 'application/json');
    }

    /**
     * Error Response With Error Details
     * @param string $message
     * @param array $errors
     * @param int $code
     * @param string $service
     * @param boolean $status
     * @return json
     */
    public function errorResponseWithDetails($message, $errors, $code, $service = "", $status = false)
    {
        $service = env('MICROSERVICE_NAME');

        return response()->json(
                            [
                                'code' => $code,
                                'status' => $status,
                                'service' => $service,
                                'message' => $message, 
                                'errors' => $errors
                            ], 
                            $code
                        );
    }

    /**
     * Error Response Without Error Details
     * @param string $message
     * @param int $code
     * @param string $service
     * @param boolean $status
     * @return json
     */
    public function errorResponseWithoutDetails($message, $code, $service = "", $status = false)
    {
        $service = env('MICROSERVICE_NAME');

        return response()->json(
                            [
                                'code' => $code,
                                'status' => $status,
                                'service' => $service,
                                'message' => $message
                            ], 
                            $code
                        );
    }

    /**
     * Error Message
     * @param string $message
     * @param int $code
     * @return json
     */
    public function errorMessage($message, $code)
    {
        return response($message, $code)->header('Content-Type', 'application/json');
    }
}