<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\BranchRepository;
use App\Repositories\DepartmentRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\Interfaces\BranchInterface;
use App\Repositories\Interfaces\DepartmentInterface;
use App\Repositories\Interfaces\OrganizationInterface;

class OrganizationServiceProvider extends ServiceProvider
{
    /**
     * Register branch and organization repository services
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            BranchInterface::class, 
            BranchRepository::class
        );

        $this->app->bind(
            OrganizationInterface::class, 
            OrganizationRepository::class
        );

        $this->app->bind(
            DepartmentInterface::class, 
            DepartmentRepository::class
        );
    }
}
