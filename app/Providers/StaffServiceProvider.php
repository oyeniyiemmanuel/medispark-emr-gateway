<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\StaffRepository;
use App\Repositories\Interfaces\StaffInterface;

class StaffServiceProvider extends ServiceProvider
{
    /**
     * Register Staff repository service
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            StaffInterface::class, 
            StaffRepository::class
        );
    }
}