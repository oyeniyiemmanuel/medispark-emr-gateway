<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\PatientRepository;
use App\Repositories\Interfaces\PatientInterface;

class PatientServiceProvider extends ServiceProvider
{
    /**
     * Register Patient repository service
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PatientInterface::class, 
            PatientRepository::class
        );
    }
}