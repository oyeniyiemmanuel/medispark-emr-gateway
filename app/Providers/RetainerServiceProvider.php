<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\RetainerRepository;
use App\Repositories\Interfaces\RetainerInterface;

class RetainerServiceProvider extends ServiceProvider
{
    /**
     * Register Retainer repository service
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            RetainerInterface::class, 
            RetainerRepository::class
        );
    }
}
