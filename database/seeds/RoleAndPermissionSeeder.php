<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        /**
        * NOTE: For the clinical-investigation REQUESTING END, whoever has the persmission to "read clinical-investigation" also has the others like "read prescription", "read lab-investigation" etc. although, the reverse isn't the case. In the same vein, whoever has the permission to "write clinical-investigation" also has the others like "write prescription", "write lab-investigation" etc. although, the reverse isn't the case.

         However, on the clinical-investigation PROCESSING END, Nobody has all permissions like "write dispensary", "write lab-result", "write imaging-result" etc. except for the super-admin.

         lastly, whoever has "read clinical-history" can read both the requesting and processing ends of clinical-investigation
        */

        // create permissions to read
        Permission::create(['name' => 'read patient-biodata']);
        Permission::create(['name' => 'read billing']);
        Permission::create(['name' => 'read comment']);
        Permission::create(['name' => 'read admit-discharge']);
        Permission::create(['name' => 'read file-upload']);
        Permission::create(['name' => 'read immunization']);
        Permission::create(['name' => 'read prescription']); // a clinical-investigation requesting End
        Permission::create(['name' => 'read dispensary']); // a processing End
        Permission::create(['name' => 'read lab-investigation']); // a clinical-investigation requesting End
        Permission::create(['name' => 'read lab-result']); // a processing End
        Permission::create(['name' => 'read imaging-investigation']); // a clinical-investigation requesting End
        Permission::create(['name' => 'read imaging-result']); // a processing End
        Permission::create(['name' => 'read clinical-investigation']); // all clinical-investigation requesting Ends
        Permission::create(['name' => 'read clinical-history']);
        Permission::create(['name' => 'read encounter']);
        Permission::create(['name' => 'read vitals']);
        Permission::create(['name' => 'read management-report']);
        Permission::create(['name' => 'read organization-config']);
        Permission::create(['name' => 'read accounting']);
        Permission::create(['name' => 'read procedure']);
        Permission::create(['name' => 'read insurance']);
        Permission::create(['name' => 'read messaging']);

        // create permissions to write
        Permission::create(['name' => 'write patient-biodata']);
        Permission::create(['name' => 'write billing']);
        Permission::create(['name' => 'write comment']);
        Permission::create(['name' => 'write admit-discharge']);
        Permission::create(['name' => 'write file-upload']);
        Permission::create(['name' => 'write immunization']);
        Permission::create(['name' => 'write prescription']); // a clinical-investigation requesting End
        Permission::create(['name' => 'write dispensary']); // a processing End
        Permission::create(['name' => 'write lab-investigation']); // a clinical-investigation requesting End
        Permission::create(['name' => 'write lab-result']); // a processing End
        Permission::create(['name' => 'write imaging-investigation']); // a clinical-investigation requesting End
        Permission::create(['name' => 'write imaging-result']); // a processing End
        Permission::create(['name' => 'write clinical-investigation']); // all clinical-investigation requesting Ends
        Permission::create(['name' => 'write clinical-history']);
        Permission::create(['name' => 'write encounter']);
        Permission::create(['name' => 'write vitals']);
        Permission::create(['name' => 'write management-report']);
        Permission::create(['name' => 'write organization-config']);
        Permission::create(['name' => 'write accounting']);
        Permission::create(['name' => 'write procedure']);
        Permission::create(['name' => 'write insurance']);
        Permission::create(['name' => 'write messaging']);

        /**
        * CREATE DOCTOR'S ROLE AND ADD PERMISSION
        */
        $role = Role::create(['name' => 'doctor']);
        $role->givePermissionTo('read clinical-history');
        $role->givePermissionTo('read clinical-investigation');
        $role->givePermissionTo('read comment');
        $role->givePermissionTo('read immunization');
        $role->givePermissionTo('read encounter');
        $role->givePermissionTo('read procedure');

        // super admin
        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}