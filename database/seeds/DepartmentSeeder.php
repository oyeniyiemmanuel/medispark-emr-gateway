<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    public function run()
    {
        // Create default departments
        Department::create(['name' => 'nursing']);
        Department::create(['name' => 'medical']);
        Department::create(['name' => 'accounts']);
        Department::create(['name' => 'pharmacy']);
        Department::create(['name' => 'laboratory']);
        Department::create(['name' => 'imaging']);
        Department::create(['name' => 'health']);
        Department::create(['name' => 'billing']);
        Department::create(['name' => 'maintenance']);
        
    }
}