<?php

/**
* ROUTES THAT DO NOT NEED TOKEN AUTHORIZATION OR LOGIN
*/

// routes only accessible to cloud intalled app
$router->group(['middleware' => ['cloud-installation']], function() use ($router) {
	$router->post('/register','user\UserController@register');
});

$router->post('/login','user\UserController@login');

$router->get('organization/{organization_slug}/login', 'organization\OrganizationController@getDetailsForLoginPage');
// after getting the details from organization/{organization_slug}/login, then send json request to oauth/token in the following format

// {
//     "grant_type": "password",
//     "client_id": 2,
//     "client_secret": "cFcle7TEpWGeBF15DM0osTFnaDkz8mGa7RkLCKc9",
//     "username": {
//         "organization_id": 4,
//         "branch_id": 2,
//         "username": "oyeniyiemmanuel@gmail.com"
//     },
//     "password": "pass"
// }

// post request to login with organization, branch and identifer details
// $router->post('organization/{organization_slug}/login', 'organization\OrganizationController@login');


/**
* TEST GATEAY INDEX
*/
$router->get('/', function () use ($router) {
    return "Gateway index";
});
	

/**
* ROUTES THAT NEED TOKEN AUTHORIZATION OR LOGIN
*/
$router->group(['middleware' => 'client'], function() use ($router){

	// routes only accessible to doctors
	$router->group(['middleware' => ['role:doctor']], function() use ($router) {
		$router->get('/encounter', function () use ($router) {
		    return "doctor access link";
		});	
	});

	// routes that start with url "clinic/"
	$router->group(['prefix' => 'clinic'], function () use ($router) {
		$router->get('/patients', 'patient\PatientController@index');

		$router->post('/vitals/new', ['middleware' => ['user-has-organization','user-has-branch'], 'uses' => 'vitals\VitalsController@newVitals']);

		$router->post('/organization/new-patient', ['middleware' => ['user-has-organization','user-has-branch'], 'uses' => 'patient\PatientController@newPatient']);

		$router->post('organization/branch/retainer/all',['middleware' => ['user-has-organization','user-has-branch'], 'uses' => 'retainer\RetainerController@getAllBranchRetainers']);

	    // routes only accessible to super-admin
		$router->group(['middleware' => ['role:super-admin']], function() use ($router) {
			// routes only accessible to super admin and cloud installed app
			$router->group(['middleware' => ['cloud-installation']], function() use ($router) {
				$router->get('organization/details', 'organization\OrganizationController@getDetails');
				$router->post('organization/new', 'organization\OrganizationController@newOrganization');

				$router->post('organization/branch/new', ['middleware' => 'user-has-organization', 'uses' => 'organization\BranchController@newBranch']);
			});

			// routes only accessible only when user has organization and branch
			$router->post('organization/branch/retainer/new',['middleware' => ['user-has-organization','user-has-branch'], 'uses' => 'retainer\RetainerController@newRetainer']);

			$router->post('organization/new-staff', ['middleware' => ['user-has-organization','user-has-branch'], 'uses' => 'staff\StaffController@newStaff']);
		});
	});

	// routes that start with url "billing/"
	$router->group(['prefix' => 'billing'], function () use ($router) {

		// middlewares for billing routes
		$router->group(['middleware' => ['user-has-organization', 'user-has-branch', 'organization-has-retainer']], function() use ($router) {
			$router->post('/service/new', 'service\ServiceController@newService');

			$router->post('/service/add-to-user', 'service\ServiceController@addServiceToUser');

			$router->post('/appointment/add-to-user', 'appointment\AppointmentController@addAppointmentToUser');

			$router->post('/form/new', 'form\FormController@newForm');

			$router->post('/form/add-field', 'form\FormController@addFormField');

			$router->post('/form/new-user-form', 'form\FormController@newUserForm');
		});
	});

});


